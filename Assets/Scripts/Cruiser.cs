using UnityEngine;
using System.Collections;

public class Cruiser : BoardSquare {

	protected override void Awake () {
		base.Awake();
		totalHits = 3;
		images [2] = Resources.Load ("Cruiser") as Texture2D;
	}

	
	protected override void onSquareSelected() {
		Cruiser[] cruisers = GameObject.FindObjectsOfType<Cruiser>() as Cruiser[];
		foreach(Cruiser cruiser in cruisers) {
			if(cruiser.hit && cruiser.Number == this.Number) {
				totalHits--;
				}
			}
		if(totalHits==0) {
			GameObject.Find("GameManager").GetComponent<GameManager>().destroyShip();
			updateEndGraphic  (this);
		}
	}	

	protected override void updateEndGraphic (BoardSquare ship)
	{
		Cruiser[] cruisers = GameObject.FindObjectsOfType<Cruiser> () as Cruiser[];
		foreach (Cruiser cruiser in cruisers) {
			if (cruiser.Number == ship.Number) {
				cruiser.GetComponent<Renderer>().material.color = new Color (1, .5f, .5f);
				cruiser.GetComponent<Renderer>().material.mainTexture = images [2];	
			}
		}
	}

}
